<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Coches;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   //cargar la pagina prncipal
        return $this->render('index');
    }
    
    public function actionCoches() {   
        // cargar un listado de todos los coches con un gridview
        $dataProvider = new ActiveDataProvider([
            "query" => Coches::find(),
        ]);
                
        return $this->render('infoCoches',[
            'dataProvider' => $dataProvider,
        ]);        
    }   
    
     public function actionOfertas() {   
         // cargar un listado de todos los coches en un listview
        $dataProvider = new ActiveDataProvider([
            "query" => Coches::find()->where(["oferta"=>1]),
        ]);
                
        return $this->render('ofertaCoches',[
            'dataProvider' => $dataProvider,
        ]);        
    }
    
    public function actionVer($id) {
        // el id es el id del coche del cual quiero ver todos los datos
        
        // consulta utilizando el where con texto
       // $modelo=Coches::find()->where("id=$id")->one();
         
        // consulta utilizando el where parametrizado
        //$modelo=Coches::find()->where(["id"=>$id])->one();
        
        //consulta utilizando el metodo findOne
        // esto es muy util para buscar por la clave principal
        $modelo=Coches::findOne($id);
        
        return $this->render("ver",[
            "modelo" => $modelo,
        ]);
    }
}

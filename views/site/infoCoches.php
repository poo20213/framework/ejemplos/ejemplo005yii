<?php

use yii\grid\GridView;
use yii\helpers\Html;


echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'marca',
        'modelo',
        'precio',
        [
            'label'=>'foto', // el nombre del campo de la base de datos
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->foto;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=> 'width:205px']); 
            }
        ],
        [
          'label'=>'Acciones',
            'format'=>'raw',
            'value' => function($data){
                $url = ['site/ver', 'id'=>$data->id];
                return Html::a('Ver mas...', $url, ['class' => 'btn btn-primary']); 
            }  
        ],        
    ]
]);
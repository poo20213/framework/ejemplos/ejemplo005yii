<?php
use \yii\widgets\ListView;

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'=>'_ofertas',
    'itemOptions'=>['class' => 'card'],
    'options'=> ['class' => 'card-colums'],
    'layout' => "{items}"
]);
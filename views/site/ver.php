<?php
use yii\widgets\DetailView;
use yii\helpers\Html;
echo DetailView::widget([
    'model' => $modelo,
    'attributes' => [
        'id',
        'marca',
        'modelo',
        'precio',
        'fecha_entrada',
        'cilindrada',
        [
            'label'=>'foto', // el nombre del campo de la base de datos
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->foto;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=> 'width:205px']); 
            }
        ],
    ]
]);


